package springmain;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import bank.MiniBankHandler;
import bank.model.Account;

@RestController
@RequestMapping(ApiConstants.MINI_BANK)
public class MiniBankController {

	@RequestMapping(value = ApiConstants.REGISTER_USER, method = RequestMethod.POST)
	public String registerUser(@RequestBody Account user) {
		return MiniBankHandler.createAccount(user.getAccName(),
				user.getAccAddr(), user.getMobile());
	}

	@RequestMapping(value = ApiConstants.DEPOSIT, method = RequestMethod.POST)
	public String deposit(@RequestBody Account data) {
		return MiniBankHandler
				.depositMoney(data.getAccName(), data.getAmount());
	}

	@RequestMapping(value = ApiConstants.WITHDRAW, method = RequestMethod.POST)
	public String withdraw(@RequestBody Account data) {
		return MiniBankHandler.withdrawMoney(data.getAccName(),
				data.getAmount());
	}

	@RequestMapping(value = ApiConstants.GET_BALANCE, method = RequestMethod.POST)
	public String getBalance(@RequestBody Account user) {
		return MiniBankHandler.getBalance(user.getAccName());
	}
}
