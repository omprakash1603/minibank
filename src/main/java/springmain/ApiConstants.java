package springmain;

public class ApiConstants {
	public static final String MINI_BANK = "/minibank";

	public static final String REGISTER_USER = "/registerUser";
	public static final String DEPOSIT = "/deposit";
	public static final String WITHDRAW = "/withdraw";
	public static final String GET_BALANCE = "/getBalance";
}
