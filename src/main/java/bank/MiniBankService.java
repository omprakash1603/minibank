package bank;

import java.sql.SQLException;

import bank.exceptions.AccountCreationException;
import bank.exceptions.InsufficientBalanceException;
import bank.exceptions.InvalidAccountException;
import dao.DaoAccount;
import dao.DaoTransaction;

public class MiniBankService {
	private static final MiniBankService INSTANCE = new MiniBankService();

	private void MiniBankService() {
	}

	public static MiniBankService getInstance() {
		return INSTANCE;
	}

	public void createAccount(final String accountName, final String accAddr,
			String mobile) throws AccountCreationException, SQLException {
		DaoAccount.createAccount(accountName, accAddr, mobile);
	}

	public void depositMoney(final String accountName, final double amount)
			throws InvalidAccountException, SQLException {
		DaoTransaction.depositMoney(accountName, amount);
	}

	public void withdrawMoney(final String accountName, final double amount)
			throws InvalidAccountException, InsufficientBalanceException,
			SQLException {
		DaoTransaction.withdrawMoney(accountName, amount);
	}

	public double getBalance(final String accountName)
			throws InvalidAccountException, SQLException {
		return DaoAccount.getBalanceByAccountName(accountName);
	}
}
