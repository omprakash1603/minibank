package bank.model;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class Account {
	private String accId;
	private String accName;
	private String accAddr;
	private String mobile;
	private double currBalance;
	private double amount;

	public Account() {

	}

	public Account(String accId, String accName, String accAddr, String mobile,
			double currBalance) {
		this.accId = accId;
		this.accName = accName;
		this.accAddr = accAddr;
		this.mobile = mobile;
		this.currBalance = currBalance;
	}


	// public String getCurrentDate() {
	// Calendar cal = Calendar.getInstance();
	// cal.set(Calendar.HOUR_OF_DAY, 0);
	// cal.set(Calendar.MINUTE, 0);
	// cal.set(Calendar.SECOND, 0);
	// cal.set(Calendar.MILLISECOND, 0);
	// Date a = cal.getTime();
	// return cal.get(Calendar.DAY_OF_MONTH) + "/"
	// + (cal.get(Calendar.MONTH) + 1) + "/" + cal.get(Calendar.YEAR);
	// }
	//
}
