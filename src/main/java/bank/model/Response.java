package bank.model;

import java.util.Calendar;
import java.util.Date;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Response {
	private Object data;
	private String error;
	transient private Date startTime = Calendar.getInstance().getTime();
	transient private Date endTime;
}
