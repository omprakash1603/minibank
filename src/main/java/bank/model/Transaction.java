package bank.model;

import java.security.Timestamp;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Transaction {
	private String tId;
	private String accId;
	private String accName;
	private double amount;
	private double currBal;
	private String accType;
	private Timestamp genTime;

	public Transaction(String tId, String accId, String accName, double amount,
			double currBal, String accType, Timestamp genTime) {
		this.tId = tId;
		this.accId = accId;
		this.accName = accName;
		this.amount = amount;
		this.currBal = currBal;
		this.accType = accType;
		this.genTime = genTime;
	}

}
