package bank.model;

public enum TransactionType {
	DEPOSIT, WITHDRAW
}
