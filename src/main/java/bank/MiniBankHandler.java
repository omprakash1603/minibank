package bank;

import java.sql.SQLException;
import java.util.Calendar;

import lombok.extern.log4j.Log4j;
import bank.exceptions.AccountCreationException;
import bank.exceptions.InsufficientBalanceException;
import bank.exceptions.InvalidAccountException;
import bank.model.Response;

import com.google.gson.Gson;

@Log4j
public class MiniBankHandler {
	public static String sendresponse(Response apiResponse) {
		apiResponse.setEndTime(Calendar.getInstance().getTime());
		long diff = apiResponse.getEndTime().getTime()
				- apiResponse.getStartTime().getTime();
		log.error("TIMESTAMP : Time taken for this request is: " + diff);

		Gson gson = new Gson();
		return gson.toJson(apiResponse);
	}

	public static String createAccount(String accName, String accAddr,
			String accMobile) {
		Response apiResponse = new Response();
		try {
			MiniBankService.getInstance().createAccount(accName, accAddr,
					accMobile);
			apiResponse.setData("Account created successfully!");
		} catch (AccountCreationException e) {
			apiResponse.setError(e.getMessage());
		} catch (SQLException e) {
			apiResponse.setError(e.getMessage());
		}
		return sendresponse(apiResponse);
	}

	public static synchronized String depositMoney(String accountName,
			double amount) {
		Response apiResponse = new Response();
		try {
			MiniBankService.getInstance().depositMoney(accountName, amount);
			apiResponse.setData("Amount of Rs: " + amount
					+ " has been deposit successfully for account: "
					+ accountName);
		} catch (InvalidAccountException e) {
			apiResponse.setError(e.getMessage());
		} catch (SQLException e) {
			apiResponse.setError(e.getMessage());
		}
		return sendresponse(apiResponse);
	}

	public static synchronized String withdrawMoney(String accountName,
			double amount) {
		Response apiResponse = new Response();
		try {
			MiniBankService.getInstance().withdrawMoney(accountName, amount);
			apiResponse.setData("Amount of Rs: " + amount
					+ " has been withdrawn successfully for account: "
					+ accountName);
		} catch (InsufficientBalanceException e) {
			apiResponse.setError(e.getMessage());
		} catch (InvalidAccountException e) {
			apiResponse.setError(e.getMessage());
		} catch (SQLException e) {
			apiResponse.setError(e.getMessage());
		}
		return sendresponse(apiResponse);
	}

	public static String getBalance(String accountName) {
		Response apiResponse = new Response();
		try {
			double amount = MiniBankService.getInstance().getBalance(
					accountName);
			apiResponse.setData("Hello " + accountName
					+ ", your current balance is: " + amount);
		} catch (InvalidAccountException e) {
			apiResponse.setError(e.getMessage());
		} catch (SQLException e) {
			apiResponse.setError(e.getMessage());
		}
		return sendresponse(apiResponse);
	}
}
