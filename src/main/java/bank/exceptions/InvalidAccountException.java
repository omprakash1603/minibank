package bank.exceptions;

public class InvalidAccountException extends Exception {
	public InvalidAccountException() {
		super("Invalid account");
	}
}
