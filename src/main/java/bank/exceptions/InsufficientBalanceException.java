package bank.exceptions;

public class InsufficientBalanceException extends Exception {
	public InsufficientBalanceException() {
		super("Insufficient balance in this account");
	}
}
