package bank.exceptions;

public class AccountCreationException extends Exception {
	public AccountCreationException() {
		super("Account with this name already exists");
	}
}
