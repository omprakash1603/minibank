package bank.exceptions;

public class DailyWithdrawlLimitExceededException extends Exception {
	public DailyWithdrawlLimitExceededException() {
		super("Daily withdrawl limit exceeded!");
	}
}
