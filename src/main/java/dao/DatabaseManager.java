package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class DatabaseManager {
	private static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
	private static final String DB_URL = "jdbc:mysql://localhost/bank";
	private static final String USERNAME = "root";
	private static final String PASSWORD = "root";

	private static ThreadLocal<Connection> threadConnection = new ThreadLocal<Connection>();

	private static Connection connect() throws SQLException {
		Connection connection = null;
		try {
			Class.forName(JDBC_DRIVER);
			connection = DriverManager
					.getConnection(DB_URL, USERNAME, PASSWORD);
			connection.setAutoCommit(false);
		} catch (SQLException e) {
			throw new SQLException(e);
		} catch (ClassNotFoundException e) {
			throw new SQLException(e);
		}

		return connection;
	}

	public static Connection getConnection() throws SQLException {
		if (threadConnection.get() == null) {
			Connection connection = connect();
			threadConnection.set(connection);
		}
		return threadConnection.get();
	}

	public static void closeResource(Connection conn,
			PreparedStatement statement, ResultSet resultSet) {
		closeConnection(conn, true);
		closeStatement(statement);
		closeResultSet(resultSet);
	}

	public static void closeResource(Connection conn,
			PreparedStatement statement, ResultSet resultSet, boolean closeConn) {
		closeConnection(conn, closeConn);
		closeStatement(statement);
		closeResultSet(resultSet);
	}

	private static void closeConnection(Connection conn, boolean closeConn) {
		if (conn != null && closeConn) {
			try {
				conn.close();
			} catch (SQLException e) {
				try {
					throw new SQLException(e);
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
			}
			conn = null;
			threadConnection.set(null);
		}
	}

	public static void freeConnection() throws SQLException {
		Connection conn = threadConnection.get();
		if (conn != null) {
			try {
				conn.close();
			} catch (SQLException e) {
				throw new SQLException(e);
			} finally {
				threadConnection.set(null);
			}
		}
	}

	private static void closeStatement(PreparedStatement statement) {
		if (statement != null) {
			try {
				statement.close();
			} catch (SQLException e) {
				try {
					throw new SQLException(e);
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
			}
			statement = null;
		}
	}

	private static void closeResultSet(ResultSet resultSet) {
		if (resultSet != null) {
			try {
				resultSet.close();
			} catch (SQLException e) {
				try {
					throw new SQLException(e);
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
			}
			resultSet = null;
		}
	}
}
