package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

import lombok.extern.log4j.Log4j;
import bank.exceptions.InsufficientBalanceException;
import bank.exceptions.InvalidAccountException;
import bank.model.Account;
import bank.model.TransactionType;
@Log4j
public class DaoTransaction {

	private static final String ADD_TRANSACTION = "insert into transaction(acc_id, acc_name, amount, transaction_type, gen_time) values(?, ?, ?, ?, ?)";
	private static final String SELECT_TRANSACTIONS_BY_ACCOUNT_ID = "select amount from transaction where ac_id=? order by id desc limit 1";

	public static void depositMoney(String accountName, double amount)
			throws InvalidAccountException, SQLException {
		Connection connection = null;
		PreparedStatement statement = null;

		try {
			connection = DatabaseManager.getConnection();
			Account account = DaoAccount.getAccountByAccountName(connection,
					accountName);
			if (account == null) {
				log.error("Account with this name does not exist");
				throw new InvalidAccountException();
			}

			statement = connection.prepareStatement(ADD_TRANSACTION);
			statement.setString(1, account.getAccId());
			statement.setString(2, account.getAccName());
			statement.setDouble(3, amount);
			statement.setString(4, TransactionType.DEPOSIT.name());
			statement
					.setTimestamp(5, new Timestamp(System.currentTimeMillis()));

			int status = statement.executeUpdate();
			if (status > 0) {
				DaoAccount.updateBalanceByAccountId(connection, account,
						amount, TransactionType.DEPOSIT);
				connection.commit();
			}
		} finally {
			DatabaseManager.closeResource(connection, statement, null);
		}
	}

	public static void withdrawMoney(String accountName, double amount)
			throws InvalidAccountException, InsufficientBalanceException,
			SQLException {
		Connection connection = null;
		PreparedStatement statement = null;
		try {
			connection = DatabaseManager.getConnection();
			Account account = DaoAccount.getAccountByAccountName(connection,
					accountName);
			if (account == null) {
				log.error("Account with this name does not exist");
				throw new InvalidAccountException();
			}
			if (amount > account.getCurrBalance()) {
				log.error("Insufficient Balance");
				throw new InsufficientBalanceException();
			}

			statement = connection.prepareStatement(ADD_TRANSACTION);
			statement.setString(1, account.getAccId());
			statement.setString(2, account.getAccName());
			statement.setDouble(3, amount);
			statement.setString(4, TransactionType.WITHDRAW.name());
			statement
					.setTimestamp(5, new Timestamp(System.currentTimeMillis()));

			int status = statement.executeUpdate();
			if (status > 0) {
				DaoAccount.updateBalanceByAccountId(connection, account,
						amount, TransactionType.WITHDRAW);
				connection.commit();
			}
		} finally {
			DatabaseManager.closeResource(connection, statement, null);
		}
	}

	public static Double getBalance(String accountName)
			throws InvalidAccountException, SQLException {
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet rs = null;
		try {
			connection = DatabaseManager.getConnection();
			statement = connection
					.prepareStatement(SELECT_TRANSACTIONS_BY_ACCOUNT_ID);
			statement.setString(1, accountName);
			rs = statement.executeQuery();

			if (rs.next()) {
				double amount = rs.getDouble("amount");
				return amount;
			} else {
				log.error("Account with this Name does not exist");
				throw new InvalidAccountException();
			}
		} finally {
			DatabaseManager.closeResource(connection, statement, rs);
		}
	}
}
