package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;

import lombok.extern.log4j.Log4j;
import bank.exceptions.AccountCreationException;
import bank.exceptions.InvalidAccountException;
import bank.model.Account;
import bank.model.TransactionType;

@Log4j
public class DaoAccount {
	private static final String ADD_ACCOUNT = "insert into accounts(acc_name, address, mobile, balance) values(?, ?, ?, ?)";
	private static final String GET_ACCOUNT_BY_ACC_NAME = "select * from accounts where acc_name = ?";
	private static final String GET_BALANCE_BY_ACC_NAME = "select balance from accounts where acc_name = ?";

	private static final String UPDATE_BALANCE_BY_ACCOUNT_ID = "update accounts set balance = ? where acc_name = ?";

	public static void createAccount(String accountName, String accAddr,
			String mobile) throws AccountCreationException, SQLException {
		Connection connection = null;
		PreparedStatement statement = null;
		try {
			connection = DatabaseManager.getConnection();
			Account account = getAccountByAccountName(connection, accountName);
			if (account != null) {
				log.error("Account with this name already exist");
				throw new AccountCreationException();
			}
			String accountId = UUID.randomUUID().toString();
			account = new Account(accountId, accountName, accAddr, mobile, 0);

			statement = connection.prepareStatement(ADD_ACCOUNT);
			statement.setString(1, account.getAccName());
			statement.setString(2, account.getAccAddr());
			statement.setString(3, account.getMobile());
			statement.setDouble(4, account.getCurrBalance());

			statement.executeUpdate();
			connection.commit();
		} finally {
			DatabaseManager.closeResource(connection, statement, null);
		}
	}

	public static Account getAccountByAccountName(Connection connection,
			String accountName) throws SQLException {
		PreparedStatement statement = null;
		ResultSet rs = null;
		boolean closeConn = false;
		try {
			if (connection == null) {
				connection = DatabaseManager.getConnection();
				closeConn = true;
			}
			statement = connection.prepareStatement(GET_ACCOUNT_BY_ACC_NAME);
			statement.setString(1, accountName);
			rs = statement.executeQuery();

			if (rs.next()) {
				String accId = rs.getString("id");
				String accName = rs.getString("acc_name");
				String accAddr = rs.getString("address");
				String mobile = rs.getString("mobile");
				double currBalance = rs.getDouble("balance");
				Account account = new Account(accId, accName, accAddr, mobile,
						currBalance);
				return account;
			} else {
				return null;
			}
		} finally {
			DatabaseManager.closeResource(connection, statement, rs, closeConn);
		}
	}

	public static double getBalanceByAccountName(String accountName)
			throws InvalidAccountException, SQLException {
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet rs = null;
		try {
			connection = DatabaseManager.getConnection();
			statement = connection.prepareStatement(GET_BALANCE_BY_ACC_NAME);
			statement.setString(1, accountName);
			rs = statement.executeQuery();

			if (rs.next()) {
				double currBalance = rs.getDouble("balance");
				return currBalance;
			} else {
				log.error("Account does not exist.");
				throw new InvalidAccountException();
			}
		} finally {
			DatabaseManager.closeResource(connection, statement, rs);
		}
	}

	public static void updateBalanceByAccountId(Connection connection,
			Account account, double amount, TransactionType tType)
			throws SQLException {
		double updatedAmount = account.getCurrBalance();
		if (tType == TransactionType.DEPOSIT) {
			updatedAmount += amount;
		} else if (tType == TransactionType.WITHDRAW) {
			updatedAmount -= amount;
		}

		PreparedStatement statement = null;
		boolean closeConn = false;
		try {
			if (connection == null) {
				connection = DatabaseManager.getConnection();
				closeConn = true;
			}
			statement = connection
					.prepareStatement(UPDATE_BALANCE_BY_ACCOUNT_ID);
			statement.setDouble(1, updatedAmount);
			statement.setString(2, account.getAccName());

			statement.executeUpdate();
		} finally {
			DatabaseManager.closeResource(connection, statement, null,
					closeConn);
		}
	}
}
