 Below is the postman link of Request:

	https://www.getpostman.com/collections/2443d81a09cfd26d0107


1) Register User
	Request URL:
		127.0.0.1:8080/minibank/registerUser

	REQUEST Body:
		{
		"accName": "Omprakash",
		"accAddr": "Bangalore",
		"mobile":"7123456789"
		}
	
2) Deposit money
	Request URL:
		127.0.0.1:8080/minibank/deposit

	REQUEST Body:
		{
		"accName": "Omprakash",
		"amount": 5000
		}
	
3) Withdraw money
	Request URL:
		127.0.0.1:8080/minibank/withdraw

	REQUEST Body:
		{
		"accName" : "Omprakash",
		"amount": 2000
		}

4) Get Balance for a user
	Request URL:
		127.0.0.1:8080/minibank/getBalance
	
	REQUEST Body:
		{
		"accName" : "Omprakash"
		}
