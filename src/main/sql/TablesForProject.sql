create database bank;

create table accounts (id int primary key auto_increment, acc_name varchar(20), address varchar(20), mobile varchar(15), balance numeric(10,2));

create table transaction (id int primary key auto_increment, acc_id int, acc_name varchar(20), amount numeric(10,2), transaction_type varchar(20), gen_time timestamp, curr_bal numeric(10,2));
		 